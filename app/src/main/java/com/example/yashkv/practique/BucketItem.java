package com.example.yashkv.practique;

/**
 * Created by yashkv on 8/5/17.
 */

public class BucketItem {
    private String name;
    private String index;

    public BucketItem(){

    }
    public BucketItem(String index, String name){
        this.index = index;
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public String getIndex(){
        return index;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setIndex(){
        this.index = index;
    }
}
