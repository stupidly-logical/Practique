package com.example.yashkv.practique;

import java.io.Serializable;

public class MedicineObject implements Serializable, Comparable<MedicineObject>{

    private String id,name,generic,startDate,endDate,duration,agentUid,insulinType,
            providerUid,doseFrequency,doseType,comments, intakeTimeGap,providerName,agentName;
    private double Monday;
    private double Tuesday;
    private double Wednesday;
    private double Thursday;
    private double Friday;
    private double Saturday;
    private double Sunday;
    private double dose;
    private int medicineType;
    private double fasting;
    private double preBreakfast;
    private double postBreakfast;
    private double preLunch;
    private double postLunch;
    private double preDinner;
    private double postDinner;
    private double bedtime;
    private double random;

    public MedicineObject() {
    }

    public double getFasting() {
        return fasting;
    }

    public void setFasting(double fasting) {
        this.fasting = fasting;
    }

    public double getPreBreakfast() {
        return preBreakfast;
    }

    public void setPreBreakfast(double preBreakfast) {
        this.preBreakfast = preBreakfast;
    }

    public double getPostBreakfast() {
        return postBreakfast;
    }

    public void setPostBreakfast(double postBreakfast) {
        this.postBreakfast = postBreakfast;
    }

    public double getPreLunch() {
        return preLunch;
    }

    public void setPreLunch(double preLunch) {
        this.preLunch = preLunch;
    }

    public double getPostLunch() {
        return postLunch;
    }

    public void setPostLunch(double postLunch) {
        this.postLunch = postLunch;
    }

    public double getPreDinner() {
        return preDinner;
    }

    public void setPreDinner(double preDinner) {
        this.preDinner = preDinner;
    }

    public double getPostDinner() {
        return postDinner;
    }

    public void setPostDinner(double postDinner) {
        this.postDinner = postDinner;
    }

    public double getBedtime() {
        return bedtime;
    }

    public void setBedtime(double bedtime) {
        this.bedtime = bedtime;
    }

    public double getRandom() {
        return random;
    }

    public void setRandom(double random) {
        this.random = random;
    }


    public double getDose() {
        return dose;
    }

    public void setDose(double dose) {
        this.dose = dose;
    }

    public double getMonday() {
        return Monday;
    }

    public void setMonday(double monday) {
        Monday = monday;
    }

    public double getTuesday() {
        return Tuesday;
    }

    public void setTuesday(double tuesday) {
        Tuesday = tuesday;
    }

    public double getWednesday() {
        return Wednesday;
    }

    public void setWednesday(double wednesday) {
        Wednesday = wednesday;
    }

    public double getThursday() {
        return Thursday;
    }

    public void setThursday(double thursday) {
        Thursday = thursday;
    }

    public double getFriday() {
        return Friday;
    }

    public void setFriday(double friday) {
        Friday = friday;
    }

    public double getSaturday() {
        return Saturday;
    }

    public void setSaturday(double saturday) {
        Saturday = saturday;
    }

    public double getSunday() {
        return Sunday;
    }

    public void setSunday(double sunday) {
        Sunday = sunday;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGeneric() {
        return generic;
    }

    public void setGeneric(String generic) {
        this.generic = generic;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getAgentUid() {
        return agentUid;
    }

    public void setAgentUid(String agentUid) {
        this.agentUid = agentUid;
    }

    public String getInsulinType() {
        return insulinType;
    }

    public void setInsulinType(String insulinType) {
        this.insulinType = insulinType;
    }

    public String getProviderUid() {
        return providerUid;
    }

    public void setProviderUid(String providerUid) {
        this.providerUid = providerUid;
    }

    public String getDoseFrequency() {
        return doseFrequency;
    }

    public void setDoseFrequency(String doseFrequency) {
        this.doseFrequency = doseFrequency;
    }

    public String getDoseType() {
        return doseType;
    }

    public void setDoseType(String doseType) {
        this.doseType = doseType;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getIntakeTimeGap() {
        return intakeTimeGap;
    }

    public void setIntakeTimeGap(String intakeTimeGap) {
        this.intakeTimeGap = intakeTimeGap;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public int getMedicineType() {
        return medicineType;
    }

    public void setMedicineType(int medicineType) {
        this.medicineType = medicineType;
    }

    public double getDayValue(String day){
        double [] dayValueArray = getDayValueArray();
        for(int i = 0; i< dayValueArray.length;i++){
            if(dayValueArray[i] == Double.parseDouble(day)){
                return dayValueArray[i];
            }
        }
        return 0;
    }

    public double[] getDayValueArray(){
        return new double[]{getMonday(),getTuesday(),getWednesday(),getThursday(),getFriday(),getSaturday(),getSunday()};
    }

    public double[] getTimingValues(){
        return new double[] {getFasting(),getPreBreakfast(),getPostBreakfast(),getPreLunch(),getPostLunch(), getPreDinner(),getPostDinner(),getBedtime(),getRandom()};
    }

    @Override
    public int compareTo(MedicineObject medicineObject) {
        if(medicineObject.getStartDate().compareTo(getStartDate()) > 0){
            return 1;
        }else{
            return 0;
        }
    }

}