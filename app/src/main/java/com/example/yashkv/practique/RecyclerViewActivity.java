package com.example.yashkv.practique;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RecyclerViewActivity extends Activity {

    private List<BucketItem> itemList = new ArrayList<>();
    private RecyclerView recyclerView;
    private BucketItemAdapter bucketItemAdapter;

    private EditText query_input;
    private TextView warningText;
    private List<String> nameList = new ArrayList<>(Arrays.asList("Mumbai","Delhi","Kolkata","Chennai","Hyderabad","Jaipur","Jabalpur","Agra","Varanasi","Lucknow","Kanpur","Allahabad","Ahemdabad","Chandigarh","Amritsar","Aizwal"));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);


        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);

        bucketItemAdapter = new BucketItemAdapter(itemList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(bucketItemAdapter);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("RecyclerView Activity");

        warningText = (TextView)findViewById(R.id.no_entry_warning);
        Collections.sort(nameList);

        for(int index = 0; index<nameList.size();index++){
            BucketItem bucketItem;
            String name = nameList.get(index);
                bucketItem = new BucketItem(Integer.toString(index+1),name);
                itemList.add(bucketItem);
        }
        bucketItemAdapter.notifyDataSetChanged();


        query_input = (EditText)findViewById(R.id.query_box);
        query_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                warningText.setVisibility(View.INVISIBLE);
                recyclerView.setVisibility(View.INVISIBLE);
                itemList.clear();
                BucketItem bucketItem;
                int liveIndex = 0;
                for(int index = 0; index<nameList.size();index++){
                    String name = nameList.get(index);
                    if(name.toLowerCase().contains(s.toString().toLowerCase())){
                        bucketItem = new BucketItem(Integer.toString(++liveIndex),name);
                        itemList.add(bucketItem);
                    }
                }
                if(itemList.size() > 0){
                bucketItemAdapter.notifyDataSetChanged();
                    Animation fadeInAnimation = AnimationUtils.loadAnimation(getBaseContext(), R.anim.fade_in);
                    recyclerView.startAnimation(fadeInAnimation);
                    recyclerView.setVisibility(View.VISIBLE);
                }
                else{
                    recyclerView.setVisibility(View.GONE);
                    Animation fadeInAnimation = AnimationUtils.loadAnimation(getBaseContext(), R.anim.fade_in);
                    warningText.startAnimation(fadeInAnimation);
                    warningText.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case R.id.home:
                getActionBar().setDisplayHomeAsUpEnabled(false);
                getActionBar().setTitle("Practique");
                super.onBackPressed();
            default:
                getActionBar().setDisplayHomeAsUpEnabled(false);
                getActionBar().setTitle("Practique");
                super.onBackPressed();
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        getActionBar().setDisplayHomeAsUpEnabled(false);
        getActionBar().setTitle("Practique");
        super.onBackPressed();

    }
}
