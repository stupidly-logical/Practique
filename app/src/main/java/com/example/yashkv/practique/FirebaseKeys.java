package com.example.yashkv.practique;

/**
 * Created by stupid on 6/13/17.
 */

public class FirebaseKeys {

    public static final String RAs = "RAs";
    public static final String RAPins = "RA2Pins";
    public static final String RAMTP = "RAMTP";
    public static final String RPMTA = "RPMTA";
    public static final String agentProfileImages = "AgentProfileImages";
    public static final String  chatImages = "ChatImages";
    public static final String provider2UserMap = "Provider2UserMap";
    public static final String userMobile2Keys = "UserMobile2Keys";
    public static final String RUs = "RUs";
    public static final String RU2Pins = "RU2Pins";
    public static final String userProfileImages = "UserProfileImages";
    public static final String goofySubscription = "goofySubscription";
    public static final String goofyMetaData = "goofyMetaData";
    public static final String subscriptions = "Subscriptions";
    public static final String amountPaid = "amountPaid";
    public static final String subscriptionDuration= "duration";
    public static final String subscriptionStartDate = "startDate";
    public static final String subscriptionEndDate = "endDate";
    public static final String subscriptionTransactionDate = "transactionDate";
    public static final String subscriptionPaymentMode = "paymentMode";
    public static final String subscriptionPaymentCollectedBy = "paymentCollectedBy";
    public static final String subscriptionProviderUid = "providerUid";
    public static final String subscriptionProviderName = "providerName";
    public static final String subscriptionProviderQualification = "providerQualification";
    public static final String subscriptionProviderMciRegistrationNumber = "providerMciRegistrationNumber";
    public static final String subscriptionAgentName = "agentName";
    public static final String subscriptionAgentUid= "agentUid";
    public static final String subscriptionUserId = "userId";

    public static final String hcpRoot = "scoobydoobydoo";

    //Agent Profile Information
    public static final String agentsProfile = "frootyfrootyfoo";
    //Agent Basic Object
    public static final String basic = "basic";
    public static final String advance = "advance";
    public static final String agentName = "name";
    public static final String agentSex = "sex";
    public static final String agentCountryCode = "countryCode";
    public static final String agentMobileNumber = "mobileNumber";
    public static final String agentDateOfBirth = "dateOfBirth";
    public static final String agentDesignation = "designation";
    public static final String agentProviderType = "providerType";
    public static final String agentDatabaseName= "databaseInstance";
    public static final String agentPracticingSince = "practicingSince";
    public static final String providerUid = "providerUid";
    public static final String agentEducation = "education";
    public static final String agentIsAdmin = "isAdmin";
    public static final String agentMciRegistrationNumber = "mciRegistrationNumber";
    public static final String agentSpeciality = "speciality";
    public static final String agentSuperSpeciality = "superSpeciality";
    public static final String agentStateMedicalCouncil = "stateMedicalCouncil";
    public static final String slotDuration = "duration";
    public static final String bookedAppointmentSMSTiming = "bookedAppointmentSMSTiming";
    public static final String databaseInstanceDetails = "databaseInstanceDetails";

    public static final String monday = "monday";
    public static final String tuesday = "tuesday";
    public static final String wednesday = "wednesday";
    public static final String thursday = "thursday";
    public static final String friday = "friday";
    public static final String saturday = "saturday";
    public static final String sunday = "sunday";

    public static final String addresses = "addresses";
    //AddressObject
    public static final String title = "title";
    public static final String line1 = "line1";
    public static final String line2  = "line2";
    public static final String locality = "locality";
    public static final String pinCode = "pinCode";
    public static final String state = "state";
    public static final String city = "city";
    public static final String country = "country";
    public static final String isPrimary = "isPrimary";

    public static final String timings = "timings";
    //Timing
    public static final String addressId = "addressId";
    public static final String startTime = "startTime";
    public static final String endTime = "endTime";

    public static final String fees = "fees";
    //Fees Object
    public static final String feeName = "name";
    public static final String feeAmount = "amount";

    public static final String services = "services";
    //Service Object
    public static final String serviceName = "name";
    public static final String serviceIsPrimary = "isPrimary";


    //Appointment information
    public static final String appointment = "Appointment";
    public static final String list = "List";
    public static final String slots = "Slots";
    public static final String appointmentId = "appointmentId";
    public static final String countryCode = "countryCode";
    public static final String mobileNumber = "mobileNumber";
    public static final String appointmentUserName = "name";
    public static final String paidStatus = "paidStatus";
    public static final String queueNumber = "queueNumber";
    public static final String appointmentStatus = "status";
    public static final String appointmentWith = "with";
    public static final String appointmentUserSex = "sex";
    public static final String appointmentTimeSlot = "timeSlot";
    public static final String appointmentUserId = "userId";
    public static final String appointmentArrivalTime = "arrivalTime";
    public static final String appointmentScheduleTimeSlot = "scheduledTimeSlot";
    public static final String appointmentFollowUpDate = "followupDate";
    public static final String insulinChecked = "insulinChecked";
    public static final String hypoglycemiaChecked = "hypoglycemiaChecked";
    public static final String footcareChecked = "footcareChecked";
    public static final String sickdayrulesChecked = "sickdayrulesChecked";
    public static final String medicinesChecked = "medicinesChecked";
    public static final String testsChecked = "testsChecked";
    public static final String reportAppointment = "reportAppointment";
    public static final String reportAppointmentDate = "date";
    public static final String reportAppointmentTimeSlot = "timeSlot";
    public static final String appointmentCheckoutTime = "checkoutTime";
    public static final String followUp = "Followup";
    public static final String autoComplete = "Autocomplete";
    public static final String autoCompleteOccupation = "Occupation";
    public static final String autoCompleteDiseaseType = "DiseaseType";


    public static final String patientEducation = "PatientEducation";
    public static final String dataValue = "dataValue";
    public static final String dateValue = "dateValue";


    //UserObject profile Information - Keys
    public static final String userRoot = "goofygoofygoo";
    public static final String userDataRoot = "bootybootyboo";

    public static final String userKey2Mobiles = "UserKey2Mobiles";
    public static final String userId = "id";
    public static final String userName = "name";
    public static final String userSex = "sex";
    public static final String userDateOfBirth = "dateOfBirth";
    public static final String userWeight = "weight";
    public static final String userCountryCode = "countryCode";
    public static final String userMobileNumber = "mobileNumber";
    public static final String userLanguage = "language";
    public static final String userAge = "age";
    public static final String googleFitConnectedStatus = "googleFitConnectedStatus";

    //Form entry - Keys
    public static final String form = "Form";
    public static final String form1 = "Form1";
    public static final String form2 = "Form2";
    public static final String form3 = "Form3";
    public static final String form4 = "Form4";
    public static final String form5 = "Form5";
    public static final String form789 = "Form789";
    public static final String formDataValue = "dataValue";
    public static final String formTimeValue = "timeValue";
    public static final String formDateValue = "dateValue";
    public static final String diseaseType = "diseaseType";
    public static final String fatherHeight = "fatherHeight";
    public static final String motherHeight = "motherHeight";
    public static final String height = "height";
    public static final String pulse = "pulse";
    public static final String sittingSystolicBloodPressure = "sittingSystolicBloodPressure";
    public static final String sittingDiastolicBloodPressure = "sittingDiastolicBloodPressure";
    public static final String glycemicControl = "glycemicControl";
    public static final String referredByContact = "referredByContact";
    public static final String referredByName = "referredByName";
    public static final String referredByAddress = "referredByAddress";
    public static final String comments = "comments";

    //Chat - Keys
    public static final String userMessages = "user-messages";
    public static final String messages = "messages";
    public static final String fromUid = "fromUid";
    public static final String toUid = "toUid";
    public static final String agentUid = "agentUid";
    public static final String text = "text";
    public static final String timestamp = "timestamp";
    public static final String imageWidth = "imageWidth";
    public static final String imageHeight = "imageHeight";
    public static final String imagePath = "imagePath";

    public static final String server = "Server";

    //Blood Glucose - Keys
    public static final String bloodGlucose = "BloodGlucose";
    public static final String fasting = "fasting";
    public static final String postBreakfast = "postBreakfast";
    public static final String preLunch = "preLunch";
    public static final String postLunch = "postLunch";
    public static final String preDinner = "preDinner";
    public static final String postDinner = "postDinner";
    public static final String night = "night";
    public static final String rBS = "random";
    public static final String updatedAt = "updatedAt";
    public static final String bgValue = "dataValue";
    public static final String bgComments = "comments";

    //InsulinObject - Keys
    //Insulin - Keys
    public static final String insulin = "Insulin";
    public static final String insulinTaken = "InsulinTaken";
    public static final String insulinName = "name";
    public static final String insulinStartDate = "startDate";
    public static final String insulinEndDate = "endDate";
    public static final String insulinDuration = "duration";
    public static final String insulinType = "insulinType";
    public static final String insulinIntakeTimeGap = "intakeTimeGap";
    public static final String insulinDoseFrequency = "doseFrequency";
    public static final String insulinId = "insulinId";
    public static final String insulinProviderUid= "providerUid";
    public static final String insulinAgentUid= "agentUid";
    public static final String insulinProviderName = "providerName";
    public static final String insulinAgentName = "agentName";
    public static final String insulinPreBreakfastIntake = "preBreakfast";
    public static final String insulinPreLunchIntake = "preLunch";
    public static final String insulinPreDinnerIntake = "preDinner";
    public static final String insulinNightIntake = "bedtime";
    public static final String insulinComments = "comments";

    //MedicineObject - Keys
    public static final String medicine = "Medicine";
    public static final String medicineId = "id";
    public static final String medicineType = "medicineType";
    public static final String medicineIntakeTimeGap = "intakeTimeGap";
    public static final String medicineProviderName = "providerName";
    public static final String medicineAgentName = "agentName";
    public static final String medicineTaken = "MedicineTaken";
    public static final String medicineName = "name";
    public static final String medicineDuration = "duration";
    public static final String medicineGeneric= "generic";
    public static final String medicineStartDate = "startDate";
    public static final String medicineEndDate = "endDate";
    public static final String medicineDosage = "dose";
    public static final String medicineDosageType = "doseType";
    public static final String medicineDosageFrequency= "doseFrequency";
    public static final String medicineProviderUid= "providerUid";
    public static final String medicineAgentUid= "agentUid";
    public static final String medicineFasting= "fasting";
    public static final String medicineRandom= "random";
    public static final String medicinePreBreakfast = "preBreakfast";
    public static final String medicinePostBreakfast = "postBreakfast";
    public static final String medicinePreLunch = "preLunch";
    public static final String medicinePostLunch = "postLunch";
    public static final String medicinePreDinner = "preDinner";
    public static final String medicinePostDinner = "postDinner";
    public static final String medicineNight = "bedtime";
    public static final String medicineComments = "comments";
    public static final String Monday = "Monday";
    public static final String Tuesday = "Tuesday";
    public static final String Wednesday = "Wednesday";
    public static final String Thursday = "Thursday";
    public static final String Friday = "Friday";
    public static final String Saturday = "Saturday";
    public static final String Sunday = "Sunday";

    public static final String exercise = "Exercise";
    public static final String exerciseId = "exerciseId";
    public static final String hours = "hours";
    public static final String weight = "weight";
    public static final String physicalActivityDatabase = "PhysicalActivityDatabase";
    public static final String activity = "activity";
    public static final String specificActivity = "specificActivity";
    public static final String mets = "mets";

    public static final String googleFit = "GoogleFit";

    public static final String foodDatabase = "FoodDatabase";
    public static final String calories = "calories";
    public static final String carbs = "carbs";
    public static final String dfiber = "dfiber";
    public static final String fats = "fats";
    public static final String foodItemName = "name";
    public static final String foodItemPrimaryName = "primaryName";
    public static final String proteins = "proteins";
    public static final String foodItemType = "type";
    public static final String servingQty = "servingQty";
    public static final String servingType = "servingType";
    public static final String servingUnitQty = "servingUnitQty";
    public static final String servingUnitType = "servingUnitType";

    public static final String meal = "Meal";
    public static final String mealType = "mealType";
    public static final String mealId = "mealId";
    public static final String snacks = "Snacks";
    public static final String breakfast = "Breakfast";
    public static final String lunch = "Lunch";
    public static final String dinner = "Dinner";

    public static final String bloodGlucoseModule = "bloodGlucose";
    public static final String bloodGlucoseFastingTargetMin = "bloodGlucoseFastingTargetMin";
    public static final String bloodGlucoseFastingTargetMax = "bloodGlucoseFastingTargetMax";
    public static final String bloodGlucosePPBSTargetMin = "bloodGlucosePPBSTargetMin";
    public static final String bloodGlucosePPBSTargetMax = "bloodGlucosePPBSTargetMax";
    public static final String bloodPressureModule = "bloodPressure";
    public static final String calorieBurnGoal = "calorieBurnGoal";
    public static final String calorieIntakeGoal = "calorieIntakeGoal";
    public static final String happinessModule = "happiness";
    public static final String mealsAndExerciseModule= "mealsAndExercise";
    public static final String chatModule = "chat";
    public static final String remoteConsultationModule = "remoteConsultation";

    public static final String medicalRecords = "MedicalRecords";
    public static final String medicalRecordsAgentName = "agentName";
    public static final String medicalRecordsAgentUid = "agentUid";
    public static final String medicalRecordsClinicalNoteSummary = "clinicalNote_summary";
    public static final String medicalRecordsDiagnosis = "diagnosis";
    public static final String medicalRecordsFollowupDate = "followUpDate";
    public static final String medicalRecordsHistoryOf = "history_of";
    public static final String medicalRecordsInvestigationsAndReports = "investigationsAndReports";
    public static final String medicalRecordsOnExamination = "on_Examination";
    public static final String medicalRecordsProviderUid = "providerUid";
    public static final String medicalRecordsProviderMciRegistrationNumber = "providerMciRegistrationNumber";
    public static final String medicalRecordsProviderName = "providerName";
    public static final String medicalRecordsProviderQualification = "providerQualification";
    public static final String medicalRecordsRecordDate = "recordDate";
    public static final String medicalRecordsSpecialAdvice = "specialAdvice";
    public static final String medicalRecordsComplainOf = "complain_of";

}
