package com.example.yashkv.practique;

/**
 * Created by stupid on 6/12/17.
 */

public class Keys {

    //not used
    public static final String userKey2Mobiles = "UserKey2Mobiles";
    public static final String userId = "id";
    public static final String userName = "name";
    public static final String userSex = "sex";
    public static final String userDateOfBirth = "dateOfBirth";
    public static final String userWeight = "weight";
    public static final String userCountryCode = "countryCode";
    public static final String userMobileNumber = "mobileNumber";
    public static final String userLanguage = "language";
    public static final String userAge = "age";
    public static final String googleFitConnectedStatus = "googleFitConnectedStatus";
    //not used
    public static final String medicine = "Medicine";
    public static final String medicineId = "id";
    public static final String medicineType = "medicineType";
    public static final String medicineIntakeTimeGap = "intakeTimeGap";
    public static final String medicineProviderName = "providerName";
    public static final String medicineAgentName = "agentName";
    //not used
    public static final String medicineTaken = "MedicineTaken";
    public static final String medicineName = "name";
    public static final String medicineDuration = "duration";
    public static final String medicineGeneric= "generic";
    public static final String medicineStartDate = "startDate";
    public static final String medicineEndDate = "endDate";
    public static final String medicineDosage = "dose";
    public static final String medicineDosageType = "doseType";
    public static final String medicineDosageFrequency= "doseFrequency";
    public static final String medicineProviderUid= "providerUid";
    public static final String medicineAgentUid= "agentUid";
    public static final String medicineFasting= "fasting";
    public static final String medicineRandom= "random";
    public static final String medicinePreBreakfast = "preBreakfast";
    public static final String medicinePostBreakfast = "postBreakfast";
    public static final String medicinePreLunch = "preLunch";
    public static final String medicinePostLunch = "postLunch";
    public static final String medicinePreDinner = "preDinner";
    public static final String medicinePostDinner = "postDinner";
    public static final String medicineInsulinType = "Insulin Type";
    public static final String medicineNight = "bedtime";
    public static final String medicineComments = "comments";
    public static final String Monday = "Monday";
    public static final String Tuesday = "Tuesday";
    public static final String Wednesday = "Wednesday";
    public static final String Thursday = "Thursday";
    public static final String Friday = "Friday";
    public static final String Saturday = "Saturday";
    public static final String Sunday = "Sunday";

}
