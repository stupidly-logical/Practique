package com.example.yashkv.practique;

/**
 * Created by yashkv on 4/5/17.
 */

import android.content.Intent;
<<<<<<< HEAD
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
=======
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
>>>>>>> 813d459e314fa0e7bece247c52be059987339a07
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

<<<<<<< HEAD
import java.util.HashMap;


public class MainActivity extends FragmentActivity {

    private static final String TAG = "";
    Button rlf_btn,llf_btn,recycler_view_activity_button,recycler_view_fragment_button;
    Button fab1;

    private HashMap<String,Object> checkRequiredFields(boolean hasAllRequiredFields, HashMap<String, Object> hashMap) {

        if (hasAllRequiredFields == true)
            return hashMap;
        else
            return null;

    }


    private HashMap<String,Object> userObjectToHashMap(UserObject user) {

        HashMap<String,Object> hashMap = new HashMap<>();
        boolean hasAllRequiredFields = true;

        if (user.getAge() >= 0){
            hashMap.put(Keys.userAge,user.getAge());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Age is null");
        }

        if (user.getCountryCode() != null){
            hashMap.put(Keys.userCountryCode,user.getCountryCode());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Country Code is null");
        }

        if (user.getDateOfBirth() != null){
            hashMap.put(Keys.userDateOfBirth,user.getDateOfBirth());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"DOB is null");
        }

        if (user.getGoogleFitConnectedStatus() != null){
            hashMap.put(Keys.googleFitConnectedStatus,user.getGoogleFitConnectedStatus());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Google Fit is not connected");
        }

        if (user.getLanguage() != null){
            hashMap.put(Keys.userLanguage,user.getLanguage());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Language is null");
        }

        if (user.getMobileNumber() != null){
            hashMap.put(Keys.userMobileNumber,user.getMobileNumber());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Mobile number is null");
        }

        if (user.getName() != null){
            hashMap.put(Keys.userName,user.getName());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Name is null");
        }

        if (user.getSex() != null){
            hashMap.put(Keys.userSex,user.getSex());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Sex is null");
        }

        if (user.getUserId() != null){
            hashMap.put(Keys.userId,user.getUserId());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"ID is null");
        }

        if (user.getWeight() != null){
            hashMap.put(Keys.userWeight,user.getWeight());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Weight is null");
        }

        return checkRequiredFields(hasAllRequiredFields,hashMap);

    }

    private HashMap<String,Object> medicineObjectToHashMap(MedicineObject medicineObject) {

        HashMap<String,Object> hashMap = new HashMap<>();
        boolean hasAllRequiredFields = true;

        if (medicineObject.getName() != null){
            hashMap.put(Keys.medicineName,medicineObject.getName());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Name is null");
        }

        if (medicineObject.getAgentName() != null){
            hashMap.put(Keys.medicineAgentName,medicineObject.getAgentName());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Agent Name is null");
        }

        if (medicineObject.getAgentUid() != null){
            hashMap.put(Keys.medicineAgentUid,medicineObject.getAgentUid());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Agent UID is null");
        }

        if (medicineObject.getComments() != null){
            hashMap.put(Keys.medicineComments,medicineObject.getComments());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Comments is null");
        }

        if (medicineObject.getDose() > 0){
            hashMap.put(Keys.medicineDosage,medicineObject.getDose());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Dose is null");
        }

        if (medicineObject.getDoseFrequency() != null){
            hashMap.put(Keys.medicineDosageFrequency,medicineObject.getDoseFrequency());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Frequency is null");
        }

        if (medicineObject.getDoseType() != null){
            hashMap.put(Keys.medicineDosageType,medicineObject.getDoseType());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Dose type is null");
        }

        if (medicineObject.getDuration() != null){
            hashMap.put(Keys.medicineDuration,medicineObject.getDuration());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Duration is null");
        }

        if (medicineObject.getStartDate() != null){
            hashMap.put(Keys.medicineStartDate,medicineObject.getStartDate());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Start date is null");
        }

        if (medicineObject.getEndDate() != null){
            hashMap.put(Keys.medicineEndDate,medicineObject.getEndDate());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"End date is null");
        }

        if (medicineObject.getFasting() >= 0){
            hashMap.put(Keys.medicineFasting,medicineObject.getFasting());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Fasting is null");
        }

        if (medicineObject.getGeneric() != null){
            hashMap.put(Keys.medicineGeneric,medicineObject.getGeneric());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Generic is null");
        }

        if (medicineObject.getId() != null){
            hashMap.put(Keys.medicineId,medicineObject.getId());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"ID is null");
        }

        if (medicineObject.getInsulinType() != null){
            hashMap.put(Keys.medicineInsulinType,medicineObject.getInsulinType());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Insulin type is null");
        }

        if (medicineObject.getIntakeTimeGap() != null){
            hashMap.put(Keys.medicineIntakeTimeGap,medicineObject.getIntakeTimeGap());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Intake time gap is null");
        }

        if (medicineObject.getMedicineType() >= 0){
            hashMap.put(Keys.medicineType,medicineObject.getMedicineType());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Medicine type is null");
        }

        if (medicineObject.getMonday() >= 0){
            hashMap.put(Keys.Monday,medicineObject.getMonday());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Monday is null");
        }

        if (medicineObject.getFriday() >= 0){
            hashMap.put(Keys.Friday,medicineObject.getFriday());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Friday is null");
        }

        if (medicineObject.getTuesday() >= 0){
            hashMap.put(Keys.Tuesday,medicineObject.getTuesday());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Tuesday is null");
        }

        if (medicineObject.getWednesday() >= 0 ){
            hashMap.put(Keys.Wednesday,medicineObject.getWednesday());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Wednesday is null");
        }

        if (medicineObject.getThursday() >= 0){
            hashMap.put(Keys.Thursday,medicineObject.getThursday());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Thursday is null");
        }

        if (medicineObject.getSaturday() >= 0){
            hashMap.put(Keys.Saturday,medicineObject.getSaturday());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Saturday is null");
        }

        if (medicineObject.getSunday() >= 0){
            hashMap.put(Keys.Sunday,medicineObject.getSunday());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Sunday is null");
        }


        if (medicineObject.getBedtime() >= 0){
            hashMap.put(Keys.medicineNight,medicineObject.getBedtime());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Bedtime is null");
        }

        if (medicineObject.getPostDinner() >= 0){
            hashMap.put(Keys.medicinePostDinner,medicineObject.getPostDinner());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Post dinner is null");
        }

        if (medicineObject.getPreDinner() >= 0){
            hashMap.put(Keys.medicinePreDinner,medicineObject.getPreDinner());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Pre dinner is null");
        }

        if (medicineObject.getPostLunch() >= 0){
            hashMap.put(Keys.medicinePostLunch,medicineObject.getPostLunch());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Post lunch is null");
        }

        if (medicineObject.getPreLunch() >= 0){
            hashMap.put(Keys.medicinePreLunch,medicineObject.getPreLunch());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Pre lunch is null");
        }

        if (medicineObject.getPostBreakfast() >= 0){
            hashMap.put(Keys.medicinePostBreakfast,medicineObject.getPostBreakfast());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Post breakfast is null");
        }

        if (medicineObject.getPreBreakfast() >= 0){
            hashMap.put(Keys.medicinePreBreakfast,medicineObject.getPreBreakfast());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Pre breakfast is null");
        }

        if (medicineObject.getProviderName() != null){
            hashMap.put(Keys.medicineProviderName,medicineObject.getProviderName());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Provider name is null");
        }

        if (medicineObject.getProviderUid() != null){
            hashMap.put(Keys.medicineProviderUid,medicineObject.getProviderUid());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Provider UID is null");
        }

        if (Double.toString(medicineObject.getRandom()) != null){
            hashMap.put(Keys.medicineRandom,medicineObject.getRandom());
        }
        else {
            hasAllRequiredFields = false;
            Log.e(TAG,"Random is null");
        }

        return checkRequiredFields(hasAllRequiredFields,hashMap);

    }

    private void testerFunction() {
/*
        UserObject user = new UserObject();

        user.setAge(18);
        user.setCountryCode("123");
        user.setDateOfBirth("1234");
        user.setGoogleFitConnectedStatus("sdf");
        user.setLanguage("HIndi");
        user.setMobileNumber("12345");
        user.setName("qwerty");
        user.setSex("male");
        user.setUserId("123");
        user.setWeight(2345.0);*/

        MedicineObject medicineObject = new MedicineObject();

        medicineObject.setId("123");
        medicineObject.setName("25432");
        medicineObject.setAgentName("25432");
        medicineObject.setAgentUid("25432");
        medicineObject.setComments("25432");
        medicineObject.setDoseFrequency("25432");
        medicineObject.setDuration("25432");
        medicineObject.setDoseType("25432");
        medicineObject.setEndDate("25432");
        medicineObject.setStartDate("25432");
        medicineObject.setProviderUid("25432");
        medicineObject.setProviderName("25432");
        medicineObject.setIntakeTimeGap("25432");
        medicineObject.setInsulinType("25432");
        medicineObject.setGeneric("25432");
        medicineObject.setComments("25432");
        medicineObject.setAgentUid("25432");
        medicineObject.setAgentName("25432");

        medicineObject.setRandom(12.0);
        medicineObject.setFasting(12.0);
        medicineObject.setPreBreakfast(12.0);
        medicineObject.setPostBreakfast(12.0);
        medicineObject.setPreLunch(12.0);
        medicineObject.setPostLunch(12.0);
        medicineObject.setPreDinner(12.0);
        medicineObject.setPostDinner(12.0);
        medicineObject.setBedtime(12.0);
        medicineObject.setDose(12.0);
        medicineObject.setMonday(12.0);
        medicineObject.setTuesday(12.0);
        medicineObject.setWednesday(12.0);
        medicineObject.setThursday(12.0);
        medicineObject.setFriday(12.0);
        medicineObject.setSaturday(12.0);
        medicineObject.setSunday(12.0);
        medicineObject.setMedicineType(12);

        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap = medicineObjectToHashMap(medicineObject);

        Toast.makeText(this, "Some testing done", Toast.LENGTH_SHORT).show();

    }
=======

public class MainActivity extends FragmentActivity {

    Button rlf_btn,llf_btn,recycler_view_activity_button,recycler_view_fragment_button;
>>>>>>> 813d459e314fa0e7bece247c52be059987339a07

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rlf_btn = (Button)findViewById(R.id.rlf_btn);
        llf_btn = (Button)findViewById(R.id.llf_btn);
<<<<<<< HEAD
        fab1 = (Button) findViewById(R.id.testButton);
        recycler_view_activity_button = (Button)findViewById(R.id.recycler_view_activity_button);
        recycler_view_fragment_button = (Button)findViewById(R.id.recycler_view_fragment_button);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                testerFunction();
            }
        });
=======
        recycler_view_activity_button = (Button)findViewById(R.id.recycler_view_activity_button);
        recycler_view_fragment_button = (Button)findViewById(R.id.recycler_view_fragment_button);
>>>>>>> 813d459e314fa0e7bece247c52be059987339a07

        rlf_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActionBar().setDisplayHomeAsUpEnabled(true);
                getActionBar().setTitle("Relative Layout Fragment");
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_holder, new RelativeLayoutFragment());
                ft.addToBackStack("tag");
                ft.commit();

            }
        });

        llf_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActionBar().setDisplayHomeAsUpEnabled(true);
                getActionBar().setTitle("Linear Layout Fragment");
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_holder, new LinearLayoutFragment());
                ft.addToBackStack("tag");
                ft.commit();


            }
        });

        recycler_view_fragment_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActionBar().setDisplayHomeAsUpEnabled(true);
                getActionBar().setTitle("RecyclerView Fragment");
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_holder, new RecyclerViewFragment());
                ft.addToBackStack("tag");
                ft.commit();

            }
        });

        recycler_view_activity_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,RecyclerViewActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case R.id.home:
                    getActionBar().setDisplayHomeAsUpEnabled(false);
                    getActionBar().setTitle("Practique");
                    super.onBackPressed();
            default:
                getActionBar().setDisplayHomeAsUpEnabled(false);
                getActionBar().setTitle("Practique");
                super.onBackPressed();
        }

        return false;
    }

    @Override
    public void onBackPressed() {
            getActionBar().setDisplayHomeAsUpEnabled(false);
        getActionBar().setTitle("Practique");
            super.onBackPressed();

    }
}
