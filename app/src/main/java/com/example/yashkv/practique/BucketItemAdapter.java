package com.example.yashkv.practique;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by yashkv on 8/5/17.
 */

public class BucketItemAdapter extends RecyclerView.Adapter<BucketItemAdapter.MyViewHolder> {

    private List<BucketItem> itemList;

    public class MyViewHolder extends RecyclerView.ViewHolder{

        public TextView index,name;

        public MyViewHolder(View itemView) {
            super(itemView);
            index = (TextView)itemView.findViewById(R.id.row_index);
            name = (TextView)itemView.findViewById(R.id.row_name);
        }
    }

    public BucketItemAdapter(List<BucketItem> itemList){
        this.itemList = itemList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        BucketItem bucketItem = itemList.get(position);
        holder.index.setText(bucketItem.getIndex());
        holder.name.setText(bucketItem.getName());

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

}
