package com.example.yashkv.practique;

import java.io.Serializable;

public class UserObject implements Serializable {
    private String name;
    private String sex;
    private String dateOfBirth;
    private String countryCode;
    private String mobileNumber;
    private String userId;
    private String language;
    private int age;
    private String googleFitConnectedStatus;

    public String getGoogleFitConnectedStatus() {
        return googleFitConnectedStatus;
    }

    public void setGoogleFitConnectedStatus(String googleFitConnectedStatus) {
        this.googleFitConnectedStatus = googleFitConnectedStatus;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    private Double weight;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UserObject(){}
}